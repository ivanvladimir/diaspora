import os
from tqdm import tqdm
import pandas
import sys
import getopt
import json
import pandas as pd

def gen_csv(org_path,output_csv,append=False):
    """
    Main function that reads all json files from a path
    then generates a big csv file with all the entries from the Json object
    Parameters:
        org_path: a parent directory of directories that cointains
                  the json files of the dataset
        output_csv: the name of the final csv file
        append: if true the output file will contain the content of the output file 
                plus the record from the jsons
    """
    org_path=org_path
    docs=[]
    print("READING DOCUMENTS")
    for path in tqdm(os.listdir(org_path)):
        for doc in os.listdir(org_path+path):
            doc_obj=open(org_path+path+'/'+doc,'r')
            json_doc=json.loads(doc_obj.read())
            docs=docs+json_doc[list(json_doc.keys())[0]]
    df = pd.DataFrame(docs)  
    if append==True:
        org_df=pd.read_csv(output_csv)
        df=pd.concat([org_df,df])
    df.to_csv(output_csv)
   
def main(argv):
   inputpath = ''
   outputfile = ''
   append = False
   try:
      opts, args = getopt.getopt(argv,"ai:o:",["ipath=","ofile="])
   except getopt.GetoptError:
      print("ERROR USAGE SHOULD BE:")
      print('csv_generator.py -i <inputpath> -o <outputfile> -a')
      print('or')
      print('csv_generator.py -i <inputpath> -o <outputfile> -a')
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-a':
         print('appending mode')
         append=True
      elif opt in ("-i", "--ifile"):
         inputpath = arg
      elif opt in ("-o", "--ofile"):
         outputfile = arg
   print('Input path is "', inputpath)
   print('Output file is "', outputfile)
   gen_csv(inputpath,outputfile,append)

if __name__ == "__main__":
   main(sys.argv[1:])